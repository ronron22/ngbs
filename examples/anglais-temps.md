<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [règles globales](#r%C3%A8gles-globales)
- [Ressources](#ressources)
- [Présents](#pr%C3%A9sents)
    - [Present simple (présent simple).](#present-simple-pr%C3%A9sent-simple)
        - [Utilisation](#utilisation)
        - [Forme affirmative](#forme-affirmative)
            - [Construction](#construction)
            - [Exemples](#exemples)
                - [with to have](#with-to-have)
                - [with to be](#with-to-be)
                - [then](#then)
        - [Forme négative](#forme-n%C3%A9gative)
            - [Construction](#construction-1)
            - [Exemples](#exemples-1)
        - [Forme interrogative](#forme-interrogative)
            - [Construction](#construction-2)
            - [Exemples](#exemples-2)
    - [Present continuous (présent progressif)](#present-continuous-pr%C3%A9sent-progressif)
        - [Utilisation](#utilisation-1)
            - [Construction](#construction-3)
        - [Forme affirmative](#forme-affirmative-1)
            - [Exemples](#exemples-3)
                - [autres exemples avec la forme contracté](#autres-exemples-avec-la-forme-contract%C3%A9)
        - [Forme négative](#forme-n%C3%A9gative-1)
            - [Exemples](#exemples-4)
        - [Forme interrogative](#forme-interrogative-1)
            - [Exemples](#exemples-5)
- [Passés](#pass%C3%A9s)
    - [Present perfect (présent parfait)](#present-perfect-pr%C3%A9sent-parfait)
        - [Utilisation](#utilisation-2)
        - [Construction](#construction-4)
        - [Forme affirmative](#forme-affirmative-2)
            - [Exemples](#exemples-6)
                - [avec l'auxilliaire *être* (*been*)](#avec-lauxilliaire-%C3%AAtre-been)
            - [Forme contractée](#forme-contract%C3%A9e)
            - [Exemples](#exemples-7)
        - [Forme négative](#forme-n%C3%A9gative-2)
            - [Exemples](#exemples-8)
                - [Forme contractée](#forme-contract%C3%A9e-1)
        - [Forme interrogative](#forme-interrogative-2)
            - [Exemples](#exemples-9)
    - [Simple past (préterit ou passé simple)](#simple-past-pr%C3%A9terit-ou-pass%C3%A9-simple)
        - [Utilisation](#utilisation-3)
        - [Forme affirmative](#forme-affirmative-3)
            - [Exemples](#exemples-10)
                - [et avec *avoir* (*have*)](#et-avec-avoir-have)
                - [et avec *être* (*be*)](#et-avec-%C3%AAtre-be)
                - [et avec *faire* (*do*)](#et-avec-faire-do)
        - [Forme négative](#forme-n%C3%A9gative-3)
            - [Construction](#construction-5)
            - [Exemples](#exemples-11)
            - [Forme contractée](#forme-contract%C3%A9e-2)
            - [Exemples](#exemples-12)
        - [Forme interrogative](#forme-interrogative-3)
            - [Construction](#construction-6)
            - [Exemples](#exemples-13)
        - [PASSÉ SIMPLE DE TO BE, TO HAVE, TO DO](#pass%C3%A9-simple-de-to-be-to-have-to-do)
    - [Preterit continu (passé progressif)](#preterit-continu-pass%C3%A9-progressif)
        - [Utilisation](#utilisation-4)
        - [Construction](#construction-7)
        - [Forme affirmative](#forme-affirmative-4)
            - [Exemples](#exemples-14)
    - [Past perfect](#past-perfect)
        - [Utilisation](#utilisation-5)
        - [Construction](#construction-8)
        - [Forme affirmative](#forme-affirmative-5)
            - [Exemples](#exemples-15)
- [Futurs](#futurs)
    - [the Futur (futur simple)](#the-futur-futur-simple)
        - [Utilisation](#utilisation-6)
        - [Construction](#construction-9)
        - [Forme affirmative](#forme-affirmative-6)
            - [Exemples](#exemples-16)
            - [Forme contractée](#forme-contract%C3%A9e-3)
                - [Exemples](#exemples-17)
        - [Forme négative](#forme-n%C3%A9gative-4)
            - [Exemples](#exemples-18)
                - [Forme contractée](#forme-contract%C3%A9e-4)
                    - [Exemples](#exemples-19)
        - [Forme interogative](#forme-interogative)
            - [construction](#construction)
            - [Exemples](#exemples-20)
    - [the Futur continuous ??()](#the-futur-continuous)
        - [Utilisation](#utilisation-7)
        - [Construction](#construction-10)
        - [Forme affirmative](#forme-affirmative-7)
            - [Exemples](#exemples-21)
        - [Forme négative](#forme-n%C3%A9gative-5)
            - [Exemples](#exemples-22)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# règles globales

A tous les temps *simple*, exemple *present* ou *past* *simple*,  aucun auxilliaire n'est utilisé à la forme affirmative, mais l'auxilliaire *do* est utilisé durant les deux autres formes.

# Ressources

* https://www.anglaiscours.fr/lecons-d-anglais

# Présents

## Present simple (présent simple).

### Utilisation

* pour exprimer des habitudes, des vérités générales, des actions répétées ou des situations immuables, des émotions et des désirs :
I smoke (habit); I work in London (unchanging situation); London is a large city (general truth)
* pour donner des instructions ou des directives :
You walk for two hundred meters, then you turn left.
* pour exprimer des dispositions fixes, présentes ou futures :
Your exam starts at 09.00
* pour exprimer le futur, après certaines conjonctions : after, when, before, as soon as, until:
He'll give it to you when you come next Saturday.

### Forme affirmative

#### Construction

Rien de particulier.  
Pas d'utilisation obligatoire d'un *auxilliaire* comme *être* ou *avoir*.

#### Exemples

##### with to have

* i have..
* you have..
* he has.. 
* she has..
* we have..
* you have..
* they have..

##### with to be

* i am 
* your are
* he is
* she is
* it is
* we are
* you are
* they are

##### then

* i think
* he thinks
* she thinks
* it thinks
* We think

### Forme négative

#### Construction

Les formes négatives et les questions utilisent DOES (= troisième personne de l'auxiliaire 'DO') + l'infinitif du verbe.
He wants ice cream. Does he want strawberry? He does not want vanilla.

#### Exemples

* you do not think
* he does not think
* they do not think

### Forme interrogative

#### Construction

Les formes négatives et les questions utilisent DOES (= troisième personne de l'auxiliaire 'DO') + l'infinitif du verbe.
He wants ice cream. Does he want strawberry? He does not want vanilla.

#### Exemples

* do i think ?
* does he think ?
* do they think ?
* what does the novel deal with ? (de quoi parle le roman ?)

___________________________________________________________________________________________________


## Present continuous (présent progressif)

présent en be+ing 

### Utilisation

s'utilise pour décrire quelque chose que l'on fait actuellement.

#### Construction

Le présent progressif de n'importe quel verbe est composé de deux parties : le verbe to be au présent + le participe présent du verbe principal.

### Forme affirmative

#### Exemples

* i am going
* you are being ill
* he is being ill
* she is being ill
* we are being ill
* you are being ill
* they are being ill

##### autres exemples avec la forme contracté

* i'm cooking
* you'r shopping

### Forme négative

#### Exemples

* i am not going
* she isn't talking
* they aren't being ill

### Forme interrogative

#### Exemples

* am i going ?
* is she talking ?

# Passés

Le passé peut-être relativement simple à utiliser, il faut appliquer les mêmes règles qu'en Francais, exemple :

* je suis vraiment impressionné : i'm really impressed
* je suis vraiment étonné : i am really surprised

Quand le verbe ce termine en *é*, il s'agit du passé.

## Present perfect simple (présent parfait)

### Utilisation

Le présent parfait est utilisé pour indiquer un lien entre le présent et le passé. Le temps de l'action se situe à un moment antérieur à l’instant présent, mais non spécifié, et nous sommes souvent plus intéressés par le résultat que par l'action elle-même.  
Pour résumer, il s'utilise pour décrire un évenement passé très proche ou non terminé. 

### Construction

Il se construit avec l'auxilliaire *avoir* (*have*) et le verbe au participe passé (il s'agit d'un temps spécifiquement utilisé avec un auxilliaire).  
La présences des adjectifs *ever, already, yet, just...* dans une phrase au passé impliquera l'utilisation du *present perfect*.

Ou pour simplifer **sujet** + **have au présent (have, has)**  + **verbe au passé (infinitif  + ed ou irrégulier)**   

### Forme affirmative

#### Exemples

* i have fallen
* you have dreamt
* he has lost
* she has blown
* we have cut
* you have aeten
* they have drunk

##### avec l'auxilliaire *être* (*been*)

* i have just been bitten by a dog (je viens d'être mordu par un chien)
* she has just been bitten by a chicken

#### Forme contractée

#### Exemples

* i’ve eaten my soup.
* he’s influenced my decision.

### Forme négative

#### Exemples

* i have not walked
* he has not walked

##### Forme contractée

* it hasn't sleeped the last night
* we havn't ran yesterday

### Forme interrogative

#### Exemples

* have I walked ?
* has she visited ?
* have they walked ?

___________________________________________________________________________________________________


## Present perfect progressive (ou continu)

### Utilisation

Il sert à appuyer sur la durée d’une action.  
Since et for sont des indicateurs du present perfect progressive. Since fait référence à un moment précis, for à une durée.

Sa principale différence avec le present perfect normal est qu’on l’utilise plus pour appuyer sur la durée de l’action ou sur l’action elle même, tandis qu’au present perfect on veut plutôt constater les réusltats de l’action. Ca reste dans le domaine de la nuance par rapport au present perfect, donc avec un exemple cela sera sûrement plus clair :

Si je dis :

* *I have cleaned my room* (present perfect normal). Cela veut dire que je viens de nettoyer ma chambre et que maintenant, le résultat visible est que l’on peut constater que ma chambre est propre. Le présent perfect appuie surtout sur le résultat.

Maintenant, si je dis la même phrase à la forme progressive :

exemple de present perfect progressif - *I've been cleaning my room.*

I have been cleaning my room . (present perfect progressif). Cela signifie que j’ai passé pas mal de temps à nettoyer ma chambre. [c’est pour cela que l’on rajoute souvent une durée, comme ici, for two hours]. Ce que je veux dire, ce n’est pas forcément que ma chambre est propre, c’est surtout que j’ai passé du temps à la ranger.  Donc on n’est pas sûr que j’ai fini de nettoyer ma chambre, mais on est sûr que j’y ai passé du temps.

### Construction

Pour former le present perfect progressive, on utilise have been ou has been et la forme -ing.

### Forme affirmative

#### Exemples

* i have been travelling all day : j'ai voyagé toute la journée
* She has been planning her wedding for one year : elle a planifiée son mariage depuis 1 an
* i have been waiting for 20 minutes
___________________________________________________________________________________________________


## Simple past (préterit ou passé simple)

### Utilisation

Le passé simple est utilisé pour parler d'une action terminée à un moment antérieur à l'instant présent. La durée n'a pas d'importance. Le temps de l'action peut se situer dans un passé récent ou lointain.

Règles simple :  
* *Être* au passé simple est le seul verbe à ce décliner sous deux formes : *was* et *were*.
* les auxilliaires  avoir (had) et faire (did) reste à la même forme avec tous les sujets. 

### Forme affirmative

#### Exemples

* i fell from my bicycle
* you dreamt about your house
* he lost his keys
* she blew the candels for my birthday
* yesterday, we cut paper
* last week, you ate chicken 
* one month ago, they drank a lot

##### et avec *avoir* (*have*)

* i had..
* you had..
* she had..
* we had..

##### et avec *être* (*be*)

* i was
* you were
* he was

##### et avec *faire* (*do*)

* i did
* it did
* we did
* they did

### Forme négative

#### Construction

Il se construit avec *sujet* + *did not* + *verbe à l'infinitif*  
ou
avec *sujet* + *was/were* + *verbe à l'infinitif* 

#### Exemples

* i didn't blow candle during my birthday
* they did not go
* i wasn’t happy yesterday
* i didn't have enough money for.

#### Forme contractée

#### Exemples

* she didn't like swim

### Forme interrogative

#### Construction

*Auxilliaire* + *verbe à l'infinitif sans to*

Attention, *did* ne remplace pas le verbe *do* !!!

#### Exemples

* did she arrive ?
* didn't you play ?
* did you see him ?

*did* est remplacé par *was* ou *were* lorsque le verbe *être* est présent dans la question.

* were you at work yesterday ? (avec être)
* how was your week-end ? (avec être)

*did* est remplacé par *had* lorsque le verbe *avoir* est présent dans la question (principal verbe).

* had you win last month ? (avec avoir)


### PASSÉ SIMPLE DE TO BE, TO HAVE, TO DO

|Sujet|Verbe|Verbe|Verbe|
|---|---|---|---|
| Sujet | Be | Have | Do |
| I | was | had | did |
| You| were | had | did |
| He/She/It | was | had | did |
| We | were | had | did |
| You | were | had | did |
| They | were | had | did |

___________________________________________________________________________________________________

## Preterit continu (passé progressif)

### Utilisation

Le passé progressif décrit des actions ou des événements à un moment antérieur à l'instant présent, qui ont commencé dans le passé et sont toujours en cours au moment où nous parlons. Autrement dit, il exprime une action inachevée ou incomplète dans le passé.

### Construction

(passé + ing)

### Forme affirmative

#### Exemples

* i was watching
* you were being
* he was losing
* she was blowing
* we were cutting
* you were eating
* they were drinking

* i wasn't watching
* he hasn't losing
* we weren't cutting

## Past perfect 

### Utilisation

Ce temps ne s'utilise que peu.  
S'utilise pour parler d'un passé lointain en relation avec un passé plus lointain encore.

### Construction

Il se construit avec l'auxilliaire *had* et le verbe au participe passé.  
La présences des adjectifs *ever, already, yet, just...* dans une phrase au passé impliquera l'utilisation du *present perfect*.

### Forme affirmative

#### Exemples

* i had fallen
* you had dreamt
* he had lost
* she had blown
* we had cut
* you had aeten
* they had drunk

# Futurs

## the Futur (futur simple)

### Utilisation


*Will* est employé pour parler d’un avenr quasi-certain (ex : *I will be 26 next year*).  
Il est aussi employé pour souligner l’expression de la volonté. Cela sous entend que la décision vient d’être prise. (Si la décision est antérieure au moment où l’on parle, on n’emploie pas will en général).

On emploie le futur avec "will"" pour des prévisions, des propositions, des promesses et des décisions spontanées.

### Construction

le futur se construit avec le modal *will* plus *l'infinif du verbe* sans le *to* devant

### Forme affirmative

#### Exemples

* i will go in London

#### Forme contractée

##### Exemples

* he'll come soon

### Forme négative

#### Exemples

* you will not buy something ? 

##### Forme contractée

###### Exemples

* he won’t be happy.

### Forme interogative

#### construction

Will prend le rôle d’auxiliaire, donc il n’y a pas besoin des auxiliaires *do have* ou *be*.

#### Exemples

* shall we go ? (on y va ? ou irions-nous ?)
* will you buy me some bread ? 

## the Futur continuous ??()

### Utilisation

1. on l’utilise aussi pour faire des prédictions à partir d’indices présents (ex : Be careful, you’re going to fall! fais attention, tu vas tomber ! )
2. on l’utilise enfin quand la décision est prise avant le moment où l’on parle (par opposition will qui sert à exprimer une décision venant d’être prise). Par exemple : I am going to take an English lesson.
3. enfin, be going to + verbe peut s’utiliser lorsque l’on donne un ordre, ou que l’on utilise une forme d’autorité. Par exemple: Now you’re going to get in your car and leave this town as soon as you can. Maintenant tu vas monter dans ta voiture et quitter cette ville aussi rapidement que tu peux.

4. pour exprimer un projet ou une intention, on emploie le futur avec going to. Le verbe to go, employé comme un auxiliaire, est toujours utilisé.

5. d'autre part, on emploie going to lorsque l'on voit avec précision que quelque chose est sur le point de se passer.

À noter que dans le language familier, going to se dira parfois GONNA. 

Comparaion avec le futur proche : je vais acheter, le futur simple se compare lui plutôt avec le futur (tout cour) : j'acheterai.
Je vais aller faire des cours (futur continuous) et j'acheterais du pain (futur simple).

### Construction

Il se construit avec *be* + *going to* + *verbe*, ex *be careful ! You're going to fall*

### Forme affirmative

#### Exemples

* i am going to read 

### Forme négative

#### Exemples

* she's not going to go out this evening
