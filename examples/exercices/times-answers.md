* je suis tombé de mon vélo
* tu rêvais de ta maison
* il a perdu ses clés
* elle a soufflé les bougies pour mon anniversaire
* hier, nous avons coupé du papier
* la semaine dernière, tu as mangé du poulet
* il y a un mois, ils ont beaucoup bu

* j'avais
* tu avais
* elle avait
* nous avions

* j'étais
* tu étais
* il était

* j'ai fait
* il a fait
* nous avons fait
* ils ont fait

* je n'ai pas soufflé de bougie pendant mon anniversaire
* ils ne sont pas allés
* je n'étais pas heureux hier
* elle ne les a pas eu hier

* elle n'a pas aimé nager

* est-elle arrivée ?
* tu n'as pas joué ?
* l'avez-vous vu ? 
* etiez-vous au travail hier ?
* comment était votre week-end ?
* avez-vous gagner le mois dernier ?
