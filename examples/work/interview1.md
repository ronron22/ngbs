<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Interview](#interview)
    - [Personnal answer](#personnal-answer)
        - [What are your strengths ?](#what-are-your-strengths)
        - [What are your weaknesses ?](#what-are-your-weaknesses)
        - [Tell us about your education](#tell-us-about-your-education)
        - [Where do you see yourself 5 years from now ?](#where-do-you-see-yourself-5-years-from-now)
    - [About your job and experience](#about-your-job-and-experience)
        - [Why did you leave your last job ?](#why-did-you-leave-your-last-job)
        - [Can you tell me about your present job ?](#can-you-tell-me-about-your-present-job)
        - [Well, so, and your last job or your previous experiences ?](#well-so-and-your-last-job-or-your-previous-experiences)
        - [What’s your principal achievements ?](#whats-your-principal-achievements)
        - [What kind of salary do you expect ?](#what-kind-of-salary-do-you-expect)
    - [General  answer](#general-answer)
        - [why do you want to work in Ireland ?](#why-do-you-want-to-work-in-ireland)
        - [what do you do in your free time ?](#what-do-you-do-in-your-free-time)
        - [Do you have any questions for me/us ?](#do-you-have-any-questions-for-meus ?)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Interview

## Personnal answer
 
My name is Antonio, I am 45 and come from France

### What are your strengths ?

I believe :
* i'm gifted for troubleshoot technical problems.  
* i'm a good team player,i always played in teams, but i'm also autonomous if needed.
* i enjoy the contact with a customer, but with my english Level, i think not be the natural choice for that.
* i'm pragmatic, so, able to choose between new and reliable technologies.
* i have an inquiring mind

### What are your weaknesses ?

* i'm not so efficient if the context or the purpose are not clearly define
* maybe, i can improve my organizational capacity

### Tell us about your education

* i'm bachelor's degree and self-taught/autodidact in computing, but my first jobs were quality control and CAD (Computer aided design) in industrial environment
* i did my military service for two years in the French air force as helicopter mecanician junior.

### Where do you see yourself 5 years from now ?

* i wish improve my skills
* i want to meet a future opportunities to exploit

## About your job and experience

### Why did you leave your last job ?

* i stayed only a few month at my last job because after a buy-out, the atmospher became very unconfortable because new direction several departures and new investissor, i saw in this situation an oppportunity to take time for travel and improve some personal points. 

### Can you tell me about your present job ?

* well, I stopped my last job in jully of last year for travel in japan some months.

### Well, so, and your last job or your previous experiences ?

* i worked 8 years as Linux and network administrator at Ecritel and Alterway (hosting), and also some month at Netvibes (analytic dashboard).

### What’s your principal achievement ?

* hard question, if i have to choose, i think which my principal achievement was the set up of the AMELI's web site, AMELI is an French health ministry, in this project, i was the principal engineer and i worked on the requirements definition, the architecture, the installation/provisionning and the daily exploitation.  

In this project, a wide choice of solutions were needed in various domains like supervision, automation, networking, security, hight availability, scalling etc..  
 a wide set of opensource technologies were needed with lot of work to combine them together.  
 I needed to write a rich documentation.

### What kind of salary do you expect ?

* i don't now the medium salary for this position in Ireland, make me a proposal.

## General  answer

### why do you want to work in Ireland ?

* at first, for improve my english language, next for discover this beautiful country.

### what do you do in your free time ?

* i practice iaido and some sports, i like read book and travel.

### Do you have any questions for me/us ?

* noting yet
