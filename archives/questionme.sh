#!/bin/bash - 

# évitons de terminer en erreur sur majuscule..
shopt -s nocasematch

# for android where the /tmp doesn'it exist.
if [ -d /tmp/ ] ; then
  output_directory=/tmp/
else
  output_directory="."
fi

latin_file=${output_directory}furansugo.txt
japanese_file=${output_directory}nihongo.txt

# c'est contreproductif de le supprimer..
#trap "test -f "$latin_file" -a -f "$japanese_file" && rm -f "$latin_file" "$japanese_file"" 2 3 9

generate() {
  input_file="$1"
  if ! [ -f "$input_file" ] ; then
    echo "file "$input_file" doesn't exist, quiting"
    exit 2
  fi

  awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}' "$input_file" |\
  awk -F ":" '!/^$/ {print$1}' > "$latin_file" # furansu file's
  awk -F ":" '!/^\s$/   {print$2}' $input_file | sed 's/^\s\|\s$//g' \
  >  "$japanese_file" # nihongo file's

  test -f "$latin_file" && lfnbl=$(wc -l "$latin_file" | awk '{print$1}') || exit 2
  test -f "$japanese_file" && jfnbl=$(wc -l "$japanese_file" | awk '{print$1}') || exit 2
  if (( $lfnbl != $jfnbl )) ; then
    echo "the number of lines differs"
    exit 2
  fi
}

start_questions() {
  for file in "$latin_file" "$japanese_file" ; do
    if ! [ -f "$file" ] ; then
      echo "file "$file" doesn't exist, quiting"
      exit 2
    fi
  done

  nb=0
  while read line ; do 
  nb=$(((nb+1))) ;
  furansu=$(echo "$line"); 
  nihongo=$(sed -n ${nb}p "$japanese_file");
  echo "$furansu ?" 
  if ! read reponse  </dev/tty ; then
    echo "No input" 
    exit 2
    fi    
  if [[ "$reponse" =~ "$nihongo" ]] ; then
    echo -e "bien: .""${nihongo}"".\n"
    else
    echo -e "pas bien: .${nihongo}. !\n"
  fi   
  done < "$latin_file"
}

inverse() {
  # modifier le même fichier que l'on lis, c'est tendu string..
  cat $latin_file | { sleep 1; tac > $latin_file; }
  cat $japanese_file | { sleep 1; tac > $japanese_file; }
}

export order=$(mktemp --suffix=${0/.sh/})

case $1 in
  gen)
    generate "$2"
  ;;
  go)
    start_questions
  ;;
  gyaku)
    inverse
    start_questions
    inverse
  ;;
  *)
    echo -e '- Use "gen" "file path" for generate input files.\n- Use "go" for starting test.\n- Use"gyaku" for inverse the question orderbefore using "go".\nIn all cases, use Ctrl+c for exit.\n'
  ;;
esac
