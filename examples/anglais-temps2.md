# Anglais temps

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Ressources](#ressources)
    - [Liens](#liens)
- [Présent](#pr%C3%A9sent)
    - [Present simple](#present-simple)
        - [Utilisation](#utilisation)
            - [Repère temporels](#rep%C3%A8re-temporels)
        - [Forme affirmative](#forme-affirmative)
            - [Construction](#construction)
            - [Exemples](#exemples)
        - [Forme négative](#forme-n%C3%A9gative)
            - [Construction](#construction-1)
            - [Exemples](#exemples-1)
        - [Forme interrogative](#forme-interrogative)
            - [Construction](#construction-2)
            - [Exemples](#exemples-2)
    - [Present progressif (be+ing)](#present-progressif-being)
        - [Utilisation](#utilisation-1)
            - [Repère temporels](#rep%C3%A8re-temporels-1)
            - [Construction](#construction-3)
            - [Exemples](#exemples-3)
        - [Forme affirmative](#forme-affirmative-1)
        - [Forme négative](#forme-n%C3%A9gative-1)
        - [Forme interrogative](#forme-interrogative-1)
- [Passé](#pass%C3%A9)
    - [preterit simple (past simple)](#preterit-simple-past-simple)
        - [Utilisation](#utilisation-2)
            - [Repère](#rep%C3%A8re)
        - [Forme affirmative](#forme-affirmative-2)
            - [Construction](#construction-4)
            - [Exemple](#exemple)
        - [Forme négative](#forme-n%C3%A9gative-2)
            - [Construction](#construction-5)
            - [Exemple](#exemple-1)
        - [Forme interrogative](#forme-interrogative-2)
            - [Construction](#construction-6)
            - [Exemple](#exemple-2)
    - [Present perfect simple](#present-perfect-simple)
        - [Utilisation](#utilisation-3)
            - [Repère](#rep%C3%A8re-1)
        - [Forme affirmative](#forme-affirmative-3)
            - [Construction](#construction-7)
            - [Exemple](#exemple-3)
        - [Forme négative](#forme-n%C3%A9gative-3)
            - [Construction](#construction-8)
            - [Exemple](#exemple-4)
        - [Forme interrogative](#forme-interrogative-3)
            - [Construction](#construction-9)
            - [Exemple](#exemple-5)
    - [Preterit + ing](#preterit-ing)
        - [Utilisation](#utilisation-4)
        - [Forme affirmative](#forme-affirmative-4)
            - [Construction](#construction-10)
        - [Exemple](#exemple-6)
- [Futur](#futur)
    - [Futur simple](#futur-simple)
        - [Utilisation](#utilisation-5)
        - [Forme affirmative](#forme-affirmative-5)
            - [Construction](#construction-11)
            - [Exemples](#exemples-4)
        - [Forme négative](#forme-n%C3%A9gative-4)
            - [Construction](#construction-12)
            - [Exemples](#exemples-5)
        - [Forme interrogative](#forme-interrogative-4)
            - [Construction](#construction-13)
            - [Exemples](#exemples-6)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Ressources

### Liens

https://www.youtube.com/watch?v=vVVQTwPr6fg

## Présent

### Present simple 

#### Utilisation

* habitude
* vérité général
* caractéristique

##### Repère temporels

* every
* sometimes
* usually
* always

#### Forme affirmative

##### Construction

Sujet + verbe

##### Exemples

* he plays tennis
* the sun rises in the east (habitude)

#### Forme négative

##### Construction

Sujet + do not + verbe

##### Exemples

* he doesn't play tennis
*
#### Forme interrogative

##### Construction

do (not) + sujet + verbe

##### Exemples

* does he play tennis ?

### Present progressif (be+ing)

#### Utilisation

* action en cours de déroulement
* action future
* commentaire
* jugement
* critique

##### Repère temporels

* at the moment
* now
* look

##### Construction

Sujet + BE (au présent) + verbe + ing

##### Exemples

#### Forme affirmative

* where is Mike ? He is playing tennis 
* he is playing tennis tomorrow at 2.00 (action futur)
* he is always playing tennis (commentaire)

#### Forme négative

* he isn't playing tennis

#### Forme interrogative

* is he playing tennis ?

## Passé

### preterit simple (past simple)

#### Utilisation

* action passée
* action terminée
 passé proche
* passé lointain

##### Repère

* last week
* last month
* last year
* yesterday
* in 2010
* ago

#### Forme affirmative

##### Construction

Sujet + verbe

##### Exemple

* last sunday, he played tennis and he lost the game

#### Forme négative

##### Construction

Sujet + did not + verbe

##### Exemple

* he didn't win the game

#### Forme interrogative

##### Construction

Did + Sujet + verbe

##### Exemple

* did he play tennis ?

### Present perfect simple

#### Utilisation

* Bilan au moment présent de ses expérience passées, on s'intéresse au résultat

##### Repère

* for
* since
* already (déjà)
* not yet (pas encore)
* so far (jusqu'ici)
* never
* ever

#### Forme affirmative

##### Construction

Sujet + have + participe passé.

##### Exemple

* she has played tennis since she was 10
* look, i have bought a new racket

#### Forme négative

##### Construction

Sujet + have not + participe passé.

##### Exemple

* she hasn't played tennis since she was 10

#### Forme interrogative

##### Construction

Have + sujet + participe passé.

##### Exemple

* Have she played tennis since she was 10 ?

### Preterit + ing

#### Utilisation

* action en cours de déroulement dans le passé
* deux actions simultanées dans le passé

#### Forme affirmative

##### Construction

Sujet + Be (au passé) + verbe + ing.

#### Exemple

* last sunday, he was playing tennis when it started to rain
* i was playing tennis while you were watching a movie

## Futur

### Futur simple

#### Utilisation

Décrire le future.

#### Forme affirmative

##### Construction

Sujet + will + verbe

##### Exemples

* one day, i will quit smoking

#### Forme négative

##### Construction

Sujet + will not + infinitif du verbe

##### Exemples

* she will not buy something
* she'll not buy something

#### Forme interrogative

##### Construction

Will + sujet + infinitif du verbe.

##### Exemples

Will she buy something ?
