#!/bin/bash - 

# export debug=on for enable debug
# export gyaku=on for reverse order

# évitons de terminer en erreur sur majuscule..
shopt -s nocasematch

trap script_exit QUIT TERM HUP EXIT

if [ -z "$1" ] ; then
  echo "Please specify a input file name please !"
  exit 2
else
  if [ -s "$1" ] ; then
    input_file="$1"
  else
    echo "verify your input file $input_file, please."
    exit 2
  fi
fi

print_debug() {
     if ! [ -z $debug ] ; then
          echo "counter:$counter $1"
     fi
}

script_exit() {
  if [[ -n $nbok && -n $nbnok && -n $nbnull ]] ; then
    echo -e "\nScore\nBonnes réponses: $nbok\nMauvaises réponses: $nbnok\nRéponses nulles: $nbnull"
    echo "Nombre de questions répondus:$((($nbok + $nbnok)))"
    let percent_false="100 * ($nbnok + $nbnull) / ($nbok + $nbnok + $nbnull)"
    echo "Pourçentage de questions foirées:$percent_false %"
    if (( ${percent_false:-0} > 50 )) ; then
      echo "T'es nul..va te pendre."
    fi
    exit 
  fi
}

declare -A shitsumon_array 
# sert à indexer numériquement le fichier, dans l'ordre ou les lignes apparaissent
# car de base les tableaux associatifs ne sont pas ordonnés.
# https://stackoverflow.com/questions/29161323/how-to-keep-associative-array-order-in-bash
declare -a orders 

echo "building, please wait.."

while read line ; do
  question=$(awk -F ":" '{print$1}' <<< $line)
  question="${question/- /}"
  question="${question/% /}"
  answer=$(awk -F":" '{print$2}' <<< $line)
  answer="${answer/# /}"
  shitsumon_array["$question"]="$answer"
  orders+=( "$question" )
done < $input_file

clear
export TERM=linux

counter=0
max=${#shitsumon_array[@]}
nberr=0
nbok=0
nbnull=0
nbnok=0
loop=0

for (( counter=0 ; counter<$max ; counter++  )) ; do
  question="${orders[$counter]}"
  answer="${shitsumon_array[${orders[$counter]}]}"

     if ! [ -z $gyaku ] ; then
          answer="${orders[$counter]}"
          question="${shitsumon_array[${orders[$counter]}]}"
     fi
  
     echo "${question} ?"
  
  read response

  if [ -z "$response" ] ; then
    echo "..${answer}, next.."
    nbnull=$((( $nbnull+ 1 )))
    continue
  fi

  if [[ "$answer" =~ "$response" ]] && (( $loop < 1 )); then
    echo -e "bien: .""${answer}"".\n"
    nbok=$((( $nbok + 1 )))
    nberr=0
    loop=0
    unset gyaku
  else
    echo -e "pas bien: .${answer}. !\n"
    nberr=$((( $nberr + 1 )))
    nbnok=$((( $nbnok + 1 )))

    if (( $nberr == 1 )) ; then
      counter=$((( $counter - 1 )))
      loop=$((( $loop + 1 )))
    fi
    
    if (( $loop > 1 )) ; then
      gyaku=on
      echo gyaku
    fi

    if (( $nberr >= 3 )) ; then
      counter=$((( $counter + 1 )))
      nberr=0
      unset gyaku
    fi
  fi
done
