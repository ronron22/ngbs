"""" During my previous experiences, I worked on critical Linux production environments (Web-market, Political, government ...) and I acquired strong technical skills, working with big hosting web environments.

A large part of my job was oriented to automation with Ansible/Git to create and manage the environments for our customers.

I also worked close to the developers team to provide the best service as possible. I'm able to work and understand their requirements, and provide when necessary support to debug the applications.

A few months ago I taught myself to work with Kubernetes.
I built a POC infrastructure from scratch (not GCP/AWS), to learn how it works in internal. Now I'm looking a job in this domain to improve my skill in a real production environment.

"""  (Dernier vu et corriger par un anglais)
