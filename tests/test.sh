#!/usr/bin/expect -f

trap {
   send \x03
   send_user "You pressed Ctrl+C\n"
  } SIGINT

set timeout -1

spawn bash shitsumon.sh examples/input

match_max 100000

expect -exact "différent ?\r"

send -- "chigau\r"

expect -exact "pareil ?\r"

send \x03
