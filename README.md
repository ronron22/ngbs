# shitsumon (for Linux geek)

## what's ?

*shitsumson* help you to learn japanese words (in romaji only).

*shitsumon.sh* work under bash only, the others shell are not supported (associative array etc..) . 

C'est un petit programme en console permettant de réviser son vocabulaire en *romaji*.

```bash
bash shitsumon.sh
building, please wait..
différent ?
chigau
bien: .chigau.

pareil ?
onaji
bien: .onaji.

longtemps ?
mon cul
pas bien: .nagaku. !

longtemps ?
mes fesses
pas bien: .nagaku. !

pas longtemps ?

..mijigaku , next..
beaucoup ?
^CScore
Bonnes réponses: 2
Mauvaises réponses: 2
Réponses nulles: 1
Nombre de questions répondus : 4
Pourçentage de questions foirées : 60 %
T'es nul..va te pendre.
```

### why ?

for learning quickly the vocabular

(et marre de cacher les réponses avec une serviette en papier..)

## how use it

### Input file

you should have an file which contain on the same line, one french or english word and, separate by ":", the romaji translation.
 
Example :

```bash
~$ head -3 /tmp/in 
- différent : chigau
- pareil : onaji 
- longtemps : nagaku
```

### starting the test

```bash
bash shitsumon pathtothefile.txt
```

For help you, in case of error, it renew one time the question, only one time (in reality, his use case is learning words, not testing level). 

### Shitsumon no work as i want ..

Change the code please or send me an email.

## Units tests

I used expect for generate a good response and a Ctrl+c for exiting.

### testing configuration

```bash
cat .gitlab-ci.yml 
stages:
  - unit

before_script:
   - apt-get update -qq && apt-get install -y -qq bash expect

build:
  stage: unit
  image: ruby:2.4.4
  script:
    - bash shitsumon.sh examples/input
  only:
  - master
```

### converting encodage

My telephom create 8859 encoding, the render is bad, howto fix that ? 
```bash
iconv -f iso-8859-15 -t utf-8 examples/junika > examples/junika2
```

### testing file

```bash
cat  tests/test.sh 
#!/usr/bin/expect -f

trap {
   send \x03
   send_user "You pressed Ctrl+C\n"
  } SIGINT

set timeout -1

spawn ./shitsumon.sh examples/input

match_max 100000

expect -exact "différent ?\r"

send -- "chigau\r"

expect -exact "pareil ?\r"

send \x03
```

## English howto

inverse column (FR:EN -> EN:FR) :

```bash
awk -F ":" '{print$2" : "$1}' examples/anglais2 | sed -r "s/(^\s|\s+$)//g" > examples/anglais2-reverse
```

change the order 

```bash
tac examples/anglais2 > examples/anglais2-order-reverse
```

split initial file into 10 chunks

```bash
split -n 10 -d  -a 1 examples/anglais2-reverse examples/anglais2-reverse
```
