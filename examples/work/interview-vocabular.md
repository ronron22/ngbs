    To look for a job : chercher un emploi.
    To apply for a position (as a waitress) : postuler pour un emploi (de serveuse).
    To meet the requirements listed in the job advertisement : satisfaire aux exigences précisées dans l’annonce d’emploi.
    To emphasise strengths : mettre en valeur des atouts.
    To focus on one’s achievements : faire un focus sur ses réalisations.
    Quality : qualité.
    Skill/competence: compétence.
    To succeed (in something) : réussir (dans quelque chose).
    Shortcoming : défaut.
    Failure : échec.
    To fail (in something) : échouer (dans quelque chose).
