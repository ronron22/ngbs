
## Présentez-vous ?

Je m'appelle Antonio Moscato, j'ai 46 ans, je suis administrateur système Linux et réseaux.

## Parlez-nous de vos  expérience ?

J'ai commençé par un cursus industriel, contrôles qualité et CAO, puis en autodidacte, j'ai

Les derniers postes occupés sont administrateur système et réseau chez Alterway, Ecritel et Netvibes 

## Quelles sont les principaux projets auxquel vous avez participés ?

Ingénieur principal sur le projet Ameli (service public): définition et conception, installation et infogérence.
Suivi et amélioration continu de cette plateforme.



## Pourquoi êtes vous intéressé par notre société ?

Les technologie mise en oeuvre dans votre société semblent sexy et le domaine d'activité me semble porteur.

## Pourquoi êtes vous intéressé par ce poste ?

Le poste correspond à mon domaine de compétence et d'intêret.

## Quels sont vos points forts ?

* travaille en équipe
* passionné de technologie, en particulier l'automatisation et le devops
* autonomie dans la gestion de projet
* troubleshooting Linux et réseau avancé
