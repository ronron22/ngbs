

## Present simple 

### Forme négative et interrogative

Quand le verbe être n'est pas le verbe principal de la phrase, on utilise do

#### exemples

* she doesn't go to the restorant tonight
* i don't come today
* does she go in restorant tonight ?

Par contre, si on utilise être (be), do n'est pas nécessaire

#### exemples

* she isn't kind today (elle n'est pas gentille aujourd'hui).
* aren't you portuguese

### the and thi 

devant une voyelle *the* se prononce *thi*

### an and a

devant une voyelle, on met *an* 

## articles a et an au pluriel

au pluriel, n'employons pas *a* ou *an*, mais *some*

## have et has

### forme contractés

Ne s'utilise qu'avec got apparement

* i've got a big dog
* she's got a tiny cat

## Focus sur "que"

* whereas : tandis que
* although : bien que
* aswell : ainsi que

## tell or say

### tell

tell s'emploi lorque l'on parle de quelqu'un ou à quelqu'un

you tell someone

* Yesterday, they told to Jenny that the winter will be cold

### say

say s'emploi pour parler de quelque chose

you say something

* Yesterday, she said that today the weather is nice

## Might vs could

## Le passé avec should et ought

ajouter *have* 

* the wedding was fun, you should have gone : le mariage était amusant, tu aurais du y aller


## Parler d'une habitude ou d'autrefois

utiliser "used to" pour décrire une habitude ex

* i am used to drinking too much : j'ai l'habitude de trop boire
* he is not used to working : il n'est pas habitué à travailler
* she didn't use to eating : elle n'a pas l'habitude de manger
* did she use to eating ? : a-t-elle l'habitude de manger ?
* are she use to be ill ? : est-ce qu'elle a l'habitude d'être malade ?

A la forme interrogative et si le verbe être n'est pas présent, utiliser "did". 


## Les prépositions

Si un verbe suit la préposition il est à la forme -ing.

* Instead of going to work, I stayed home : Au lieu d'aller travailler, je suis resté à la maison

